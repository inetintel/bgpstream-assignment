# BGPStream & GRIP Assignment

This project contains instructions on how to install and use [BGPStream](https://bgpstream.caida.org/). For more details, please look [here](https://gitlab.com/inetintel/bgpstream-assignment/-/wikis/BGPStream-&-GRIP-Assignment).
